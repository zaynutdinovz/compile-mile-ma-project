# importing required libraries
from PyQt5 import QtMultimedia
from PyQt5 import QtMultimediaWidgets
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import *
from PyQt5.QtMultimedia import *
from PyQt5.QtMultimediaWidgets import *
import os
import sys
import time
from PyQt5 import QtSql
from PyQt5.QtWidgets import QApplication, QDialog, QPushButton, QMessageBox
from PyQt5.QtSql import QSqlDatabase

import numpy as np
import tenserflow as tf
from keras import layers

# Main window class
class MainWindow(QMainWindow):
  
    # constructor
    def __init__(self):
        super().__init__()
  
        # setting geometry
        self.setGeometry(100, 100,
                         800, 600)
  
        # setting style sheet
        self.setStyleSheet("background : lightgrey;")
  
        # getting available cameras
        self.available_cameras = QCameraInfo.availableCameras()
  
        # if no camera found
        if not self.available_cameras:
            # exit the code
            sys.exit()
        
         # creating a status bar
        self.status = QStatusBar()
  
        # setting style sheet to the status bar
        self.status.setStyleSheet("background : white;")
  
        # adding status bar to the main window
        self.setStatusBar(self.status)
  
        # path to save
        self.save_path = ""
  
        # creating a QCameraViewfinder object
        self.viewfinder = QCameraViewfinder()
  
        # showing this viewfinder
        self.viewfinder.show()
  
        # making it central widget of main window
        self.setCentralWidget(self.viewfinder)
  
        # Set the default camera.
        self.select_camera(0)
        
        # creating a tool bar
        toolbar = QToolBar("Camera Tool Bar")
  
        # adding tool bar to main window
        self.addToolBar(toolbar)
  
        # creating a photo action to take photo
        click_action = QAction("Capture photo", self)
  
        # adding status tip to the photo action
        click_action.setStatusTip("This will capture picture")
  
        # adding tool tip
        click_action.setToolTip("Capture picture")
  
  
        # adding action to it
        # calling take_photo method
        click_action.triggered.connect(self.click_photo)
  
        # adding this to the tool bar
        toolbar.addAction(click_action)
  
        # # similarly creating action for changing save folder
        # change_folder_action = QAction("Change save location",
        #                                self)
        
        # # adding status tip
        # change_folder_action.setStatusTip("Change folder where picture will be saved saved.")
  
        # # adding tool tip to it
        # change_folder_action.setToolTip("Change save location")
  
        # # setting calling method to the change folder action
        # # when triggered signal is emitted
        # change_folder_action.triggered.connect(self.change_folder)
  
        # # adding this to the tool bar
        # toolbar.addAction(change_folder_action)
  
  
        # creating a combo box for selecting camera
        camera_selector = QComboBox()
  
        # adding status tip to it
        camera_selector.setStatusTip("Choose camera to take pictures")
  
        # adding tool tip to it
        camera_selector.setToolTip("Select Camera")
        camera_selector.setToolTipDuration(2500)
        
        # adding items to the combo box
        camera_selector.addItems([camera.description()
                                  for camera in self.available_cameras])
  
        # adding action to the combo box
        # calling the select camera method
        camera_selector.currentIndexChanged.connect(self.select_camera)
  
        # adding this to tool bar
        toolbar.addWidget(camera_selector)
  
        # setting tool bar stylesheet
        toolbar.setStyleSheet("background : white;")
  
  
  
        # setting window title
        self.setWindowTitle("Attendance checking camera")
  
        # showing the main window
        self.show()
        
    # method to select camera
    def select_camera(self, i):
  
        # getting the selected camera
        self.camera = QCamera(self.available_cameras[i])
  
        # setting view finder to the camera
        self.camera.setViewfinder(self.viewfinder)
  
        # setting capture mode to the camera
        self.camera.setCaptureMode(QCamera.CaptureStillImage)
  
        # if any error occur show the alert
        self.camera.error.connect(lambda: self.alert(self.camera.errorString()))
  
        # start the camera
        self.camera.start()
  
        # creating a QCameraImageCapture object
        self.capture = QCameraImageCapture(self.camera)
        
        # showing alert if error occur
        self.capture.error.connect(lambda error_msg, error,
                                   msg: self.alert(msg))
  
        # when image captured showing message
        self.capture.imageCaptured.connect(lambda d,
                                           i: self.status.showMessage("Image captured : " 
                                                                      + str(self.save_seq)))
  
        # getting current camera name
        self.current_camera_name = self.available_cameras[i].description()
  
        # initial save sequence
        self.save_seq = 0
        
     # method to take photo
    def click_photo(self):
  
        # time stamp
        #timestamp = time.strftime("%d-%b-%Y-%H_%M_%S")
  
        # capture the image and save it on the save path
        self.capture.capture(os.path.join("C:\\Users\\Azamat Musaev\\Desktop\\FaceApp\\application_data\\input_image\\", 
                                          "input_image.jpg" % (
            #self.current_camera_name,
            #self.save_seq,
            #timestamp
        )))
  
        # increment the sequence
        self.save_seq += 1
        #start model func call
        start_model()
        
    
        
    # # change folder method
    # def change_folder(self):
  
    #     # open the dialog to select path
    #     path = QFileDialog.getExistingDirectory(self, 
    #                                             "Picture Location", "")
  
    #     # if path is selected
    #     if path:
  
    #         # update the path
    #         self.save_path = path
  
    #         # update the sequence
    #         self.save_seq = 0
            
    # method for alerts
    def alert(self, msg):
  
        # error message
        error = QErrorMessage(self)
  
        # setting text to the error message
        error.showMessage(msg)
        
       
def preprocess(file_path):
    # Read in image from file path
    byte_img = tf.io.read_file(file_path)
    # Load in the image
    img = tf.io.decode_jpeg(byte_img)
    # Preprocessing steps - resizing the image to be 100x100x3
    img = tf.image.resize(img, (100, 100))
     # Scale image to be between 0 and 1 
    img = img/255
    # Return image
    return img 

def DBConnection(self):
        db = QSqlDatabase("QPSQL")
        db.setUserName("postgres")  
        db.setPassword("postgres")
        db.setDatabaseName("universitydb")

        if not db.open():
            print("Unable to connect.")
            print('Last error', db.lastError().text())
            sys.exit(1)

        print(db.exec("SELECT * from instructor").first())
        QMessageBox.about(self, 'Connection', 'Database Connected Successfully')        
       
 
# Siamese L1 Distance class
class L1Dist(Layer):
    
    # Init method - inheritance
    def __init__(self, **kwargs):
        super().__init__()
       
    # Magic happens here - similarity calculation
    def call(self, input_embedding, validation_embedding):
        return tf.math.abs(input_embedding - validation_embedding)
          

def verify(model, detection_threshold, verification_threshold):
    # Build results array
    results = []
    for image in os.listdir(os.path.join('application_data', 'verification_images')):
        if (image.lower().endswith(('.png', '.jpg', '.jpeg', '.tiff', '.bmp', '.gif'))):
            input_img = preprocess(os.path.join('application_data', 'input_image', 'input_image.jpg'))
            validation_img = preprocess(os.path.join('application_data', 'verification_images', image))
        
            # Make Predictions 
            result = model.predict(list(np.expand_dims([input_img, validation_img], axis=1)))
            print(image)
            print(result)
            results.append(result)

    
    # Detection Threshold: Metric above which a prediciton is considered positive 
    detection = np.sum(np.array(results) > detection_threshold)
    
    # Verification Threshold: Proportion of positive predictions / total positive samples 
    verification = detection / len(os.listdir(os.path.join('application_data', 'verification_images'))) 
    verified = verification > verification_threshold
    
    return results, verified

def start_model():
    L1Dist
    # # Reload model 
    siamese_model = tf.keras.models.load_model('siamesemodelv2.h5', 
                                    custom_objects={'L1Dist':L1Dist, 'BinaryCrossentropy':tf.losses.BinaryCrossentropy})
    
    results, verified = verify(siamese_model, 0.95, 0.10)
    # print(verified)
    
    # print("Function called")

    
    


        
# Driver code
if __name__ == "__main__" :
    
  # create pyqt5 app
  App = QApplication(sys.argv)
  
  # create the instance of our Window
  window = MainWindow()
  
  # start the app
  sys.exit(App.exec())
  